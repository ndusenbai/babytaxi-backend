from django.urls import path, include

urlpatterns = [
    path('', include('auth_user.urls')),
    path('', include('order.urls')),
]
