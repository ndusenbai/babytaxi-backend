from drf_yasg import openapi

QUERY_USER_ID = openapi.Parameter('user_id', openapi.IN_QUERY, description='id user', type=openapi.TYPE_INTEGER)

QUERY_ACTION = openapi.Parameter(
    'is_active',
    openapi.IN_QUERY,
    description='True = ACTIVATE, False = DEACTIVATE',
    type=openapi.TYPE_BOOLEAN,
)

QUERY_ORDER_TYPE = openapi.Parameter(
    'order_type',
    openapi.IN_QUERY,
    description="Order type: FEATURE = 0, WAITING = 1, IN_PROGRESS = 2, COMPLETE = 3",
    type=openapi.TYPE_INTEGER,
)

QUERY_IS_NULL_CABBIE = openapi.Parameter(
    'is_null_cabbie',
    openapi.IN_QUERY,
    description='is null cabbie',
    type=openapi.TYPE_BOOLEAN,
)

QUERY_IS_HISTORY = openapi.Parameter(
    'is_history',
    openapi.IN_QUERY,
    description='is history',
    type=openapi.TYPE_BOOLEAN,
)

QUERY_WEEK_DAYS = openapi.Parameter(
    'week_days', openapi.IN_QUERY,
    description='[1, 2, 3, 4, 5, 6, 7]',
    type=openapi.TYPE_ARRAY, items=openapi.Items(type=openapi.TYPE_INTEGER)
)

QUERY_REGULAR_TRIP_ID = openapi.Parameter('regular_trip_id', openapi.IN_QUERY, description='regular trip id', type=openapi.TYPE_INTEGER)
