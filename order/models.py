from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.db.models import CASCADE

from auth_user.models import User, Cabbie
from common.dates import server_now
from common.models import BaseModel


class OrderType(models.IntegerChoices):
    FEATURE = 0, 'feature'
    WAITING = 1, 'waiting'
    IN_PROGRESS = 2, 'in-progress'
    COMPLETE = 3, 'complete'


class Location(BaseModel):
    latitude = models.DecimalField(max_digits=22, decimal_places=6, default=0, validators=[MinValueValidator(0)])
    longitude = models.DecimalField(max_digits=22, decimal_places=6, default=0, validators=[MinValueValidator(0)])
    name = models.CharField(max_length=100, blank=True)


class Order(BaseModel):
    type = models.IntegerField(choices=OrderType.choices, validators=[MinValueValidator(0), MaxValueValidator(3)], default=OrderType.FEATURE)
    money = models.PositiveIntegerField(validators=[MinValueValidator(0)])
    time = models.TimeField(null=False)
    customer = models.ForeignKey(to=User, on_delete=CASCADE)
    cabbie = models.ForeignKey(to=Cabbie, on_delete=CASCADE, null=True)
    location_to = models.ForeignKey(to=Location, on_delete=CASCADE)
    latitude_from = models.DecimalField(max_digits=22, decimal_places=6, default=0, validators=[MinValueValidator(0)])
    longitude_from = models.DecimalField(max_digits=22, decimal_places=6, default=0, validators=[MinValueValidator(0)])
    date = models.DateField()
    regular_trip = models.BooleanField(default=False)


class WeekDayChoices(models.IntegerChoices):
    MONDAY = 1, 'monday'
    TUESDAY = 2, 'tuesday'
    WEDNESDAY = 3, 'wednesday'
    THURSDAY = 4, 'thursday'
    FRIDAY = 5, 'friday'
    SATURDAY = 6, 'saturday'
    SUNDAY = 7, 'sunday'


class TripType(models.IntegerChoices):
    WAITING = 0, 'waiting'
    ACCEPTED = 1, 'accepted'
    FOR_CONFIRM = 2, 'for-confirm'
    REJECTED = 3, 'rejected'


class RegularTrip(BaseModel):
    customer = models.ForeignKey(to=User, on_delete=CASCADE)
    start_date = models.DateField(auto_now=True, null=True)
    location_from = models.ForeignKey(to=Location, on_delete=CASCADE, related_name='from_location', null=True)
    location_to = models.ForeignKey(to=Location, on_delete=CASCADE, related_name='to_location', null=True)
    cabbie = models.ForeignKey(to=Cabbie, on_delete=CASCADE, null=True)
    type = models.IntegerField(choices=TripType.choices, validators=[MinValueValidator(0), MaxValueValidator(3)], default=TripType.WAITING)
    name = models.CharField(max_length=100, blank=True)
    time = models.TimeField()
    money = models.PositiveIntegerField(validators=[MinValueValidator(0)])
    duration = models.IntegerField(validators=[MinValueValidator(1)])


class RegularTripSchedule(BaseModel):
    trip = models.ForeignKey(to=RegularTrip, on_delete=CASCADE)
    week_day = models.IntegerField(choices=WeekDayChoices.choices, validators=[MinValueValidator(1), MaxValueValidator(7)], default=WeekDayChoices.MONDAY)
