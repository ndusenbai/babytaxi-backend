from django.contrib import admin
from order.models import Location, Order, RegularTrip, RegularTripSchedule


@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    list_display = ('id', 'latitude', 'longitude', 'name',)
    search_fields = ('name',)
    ordering = ('id', 'name')


@admin.register(Order)
class LocationAdmin(admin.ModelAdmin):
    list_display = ('id', 'type', 'customer', 'cabbie', 'date')
    raw_id_fields = ('customer', 'cabbie')
    search_fields = ('name',)
    ordering = ('id',)


@admin.register(RegularTrip)
class RegularTripAdmin(admin.ModelAdmin):
    list_display = ('id', 'customer', 'cabbie', 'type', 'name', 'money', 'duration')
    raw_id_fields = ('customer', 'cabbie')
    search_fields = ('customer', 'cabbie')
    ordering = ('id', 'type', 'customer')


@admin.register(RegularTripSchedule)
class RegularTripScheduleAdmin(admin.ModelAdmin):
    list_display = ('id', 'trip', 'week_day')
    raw_id_fields = ('trip',)
    ordering = ('id',)



