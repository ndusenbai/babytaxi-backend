from datetime import timedelta, datetime

from django.shortcuts import get_object_or_404

from common.dates import server_now
from order.models import RegularTripSchedule, RegularTrip, Order, OrderType


def create_regular_trip_schedule(data: dict, week_days: list):
    regular_trip = get_object_or_404(RegularTrip, pk=data['id'])
    for day in week_days:
        RegularTripSchedule.objects.create(trip=regular_trip, week_day=day)


def create_order_regular_trip(regular_trip: RegularTrip):

    local_now = server_now().replace(tzinfo=None)
    location_from = regular_trip.location_from
    regular_trip_schedules = [day.week_day for day in RegularTripSchedule.objects.filter(trip_id=regular_trip.id)]

    for week in range(regular_trip.duration):
        for week_schedule in range(7):
            if local_now.weekday() in regular_trip_schedules:
                order = Order()
                order.regular_trip = True
                order.type = OrderType.WAITING
                order.time = str(regular_trip.time)
                order.money = regular_trip.money
                order.customer = regular_trip.customer
                order.cabbie = regular_trip.cabbie
                order.location_to = regular_trip.location_to
                order.latitude_from = location_from.latitude
                order.longitude_from = location_from.longitude
                order.date = str(local_now.date())
                order.save()

            local_now += timedelta(days=1)
