from rest_framework import serializers

from auth_user.serializers import CabbieSerializer
from order.models import Location, Order, RegularTrip


class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = '__all__'


class OrderSerializerGet(serializers.ModelSerializer):
    cabbie = CabbieSerializer(read_only=True)
    location_to = LocationSerializer(read_only=True)

    class Meta:
        model = Order
        fields = '__all__'


class OrderSerializerPost(serializers.ModelSerializer):
    class Meta:
        model = Order
        exclude = ['date', 'time', 'cabbie', 'type', 'created_at', 'updated_at', 'customer']


class OrderSerializerPut(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ['type']


class RegularTripSerializerPost(serializers.ModelSerializer):
    class Meta:
        model = RegularTrip
        exclude = ['type', 'customer', 'cabbie']


class RegularTripSerializerGet(serializers.ModelSerializer):
    class Meta:
        model = RegularTrip
        fields = '__all__'


class RegularTripSerializerPut(serializers.ModelSerializer):
    class Meta:
        model = RegularTrip
        fields = ('type',)
