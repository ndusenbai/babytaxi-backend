from django.urls import path, include
from rest_framework.routers import DefaultRouter

from order import views


router = DefaultRouter()
router.register('location', views.LocationApiViewSet, basename='location')
router.register('order', views.OrderApiViewSet, basename='order')
router.register('regular-trip', views.RegularTripApiViewSet, basename='regular-trip')

urlpatterns = [
    path('days/regular-trip/', views.DaysRegularTripViewSet.as_view(), name='days-regular-trip'),
] + router.urls
