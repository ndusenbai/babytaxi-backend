from datetime import datetime

from django.db.models import Q
from django.shortcuts import get_object_or_404
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet, GenericViewSet

from auth_user.models import UserType, Cabbie
from common.dates import server_now
from common.manual_parameters import QUERY_ORDER_TYPE, QUERY_IS_NULL_CABBIE, QUERY_IS_HISTORY, QUERY_WEEK_DAYS, \
    QUERY_REGULAR_TRIP_ID
from order.models import Location, Order, OrderType, RegularTrip, TripType, RegularTripSchedule, WeekDayChoices
from order.serializers import LocationSerializer, OrderSerializerGet, OrderSerializerPost, OrderSerializerPut, \
    RegularTripSerializerPost, RegularTripSerializerGet, RegularTripSerializerPut
from order.services import create_regular_trip_schedule, create_order_regular_trip


class LocationApiViewSet(ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = LocationSerializer
    queryset = Location.objects.all()
    parser_classes = (MultiPartParser,)


class OrderApiViewSet(ModelViewSet):
    permission_classes = (IsAuthenticated,)
    parser_classes = (MultiPartParser,)

    def get_queryset(self):
        local_now = server_now()
        regular_trip = Order.objects.filter(regular_trip=True, type=OrderType.FEATURE, date__gte=local_now.date(), time__gte=local_now.time())
        regular_trip.update(type=OrderType.WAITING)
        return Order.objects.all()

    def get_serializer_class(self, *args, **kwargs):
        if self.request.method == 'GET':
            return OrderSerializerGet
        elif self.request.method == 'POST':
            return OrderSerializerPost
        elif self.request.method == 'PUT':
            return OrderSerializerPut
        return OrderSerializerGet

    @swagger_auto_schema(manual_parameters=[QUERY_ORDER_TYPE, QUERY_IS_NULL_CABBIE, QUERY_IS_HISTORY])
    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        user = self.request.user

        if user.type == UserType.CABBIE:
            cabbie = Cabbie.objects.filter(user=user).first()
            extra_info = cabbie.extra_info
            extra_info_documents_list = [
                extra_info.transportation_license == '',
                extra_info.identification_card == '',
                extra_info.criminal_record_certificate == '',
                extra_info.drug_dispensary_certificate == '',
                extra_info.psychiatric_clinic_certificate == '',
            ]

            if True in extra_info_documents_list or cabbie.is_active is False:
                return Response({'message': 'Доступ запрещен'}, status=status.HTTP_400_BAD_REQUEST)

        is_null_cabbie = self.request.query_params.get('is_null_cabbie')
        order_type = self.request.query_params.get('order_type')
        is_history = self.request.query_params.get('is_history')
        if is_history:
            queryset = queryset.filter(Q(customer=user) | Q(cabbie__user=user), type=OrderType.COMPLETE)

        if user.type == UserType.CUSTOMER:
            serializer = self.get_serializer(queryset, many=True)
            return Response(serializer.data)

        if order_type and 4 > int(order_type) > 0:
            queryset = queryset.filter(type=int(order_type))

        if is_null_cabbie is not None:
            is_null_cabbie = True if is_null_cabbie == 'true' else False
            queryset = queryset.filter(cabbie__isnull=is_null_cabbie)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        user = self.request.user

        if user.type == UserType.CABBIE:
            return Response({'message': 'Водитель не может делать заказ'}, status=status.HTTP_403_FORBIDDEN)
        Order.objects.filter(customer=user, type__in=[OrderType.WAITING, OrderType.IN_PROGRESS]).delete()

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.validated_data['date'] = server_now()
        serializer.validated_data['time'] = server_now().time()
        serializer.validated_data['type'] = OrderType.WAITING
        serializer.validated_data['customer'] = user
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        """
        FEATURE = 0, 'feature'
        WAITING = 1, 'waiting'
        IN_PROGRESS = 2, 'in-progress'
        COMPLETE = 3, 'complete'
        """
        cabbie = Cabbie.objects.filter(user=self.request.user)
        if cabbie.exists() is False:
            return Response({'message': 'You are not cabbie'}, status=status.HTTP_400_BAD_REQUEST)
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        serializer.validated_data['cabbie'] = cabbie.first()
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)


class RegularTripApiViewSet(ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = RegularTrip.objects.all()
    parser_classes = (MultiPartParser,)

    def get_serializer_class(self):
        if self.request.method == "POST":
            return RegularTripSerializerPost
        elif self.request.method == "GET":
            return RegularTripSerializerGet
        elif self.request.method == "PUT":
            return RegularTripSerializerPut
        return RegularTripSerializerGet

    @swagger_auto_schema(manual_parameters=[QUERY_WEEK_DAYS])
    def create(self, request, *args, **kwargs):
        user = self.request.user
        week_days = self.request.query_params.get('week_days')

        if user.type == UserType.CABBIE:
            return Response({'message': 'Водитель не может делать заказ'}, status=status.HTTP_403_FORBIDDEN)

        if week_days:
            week_days = [int(sid) for sid in week_days.split(',')]
        else:
            return Response({'message': 'Выберите дни недели'}, status=status.HTTP_400_BAD_REQUEST)

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.validated_data['type'] = TripType.WAITING
        serializer.validated_data['customer'] = user
        self.perform_create(serializer)
        create_regular_trip_schedule(serializer.data, week_days)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        cabbie = Cabbie.objects.filter(user=self.request.user)
        if cabbie.exists() is False:
            return Response({'message': 'You are not cabbie'}, status=status.HTTP_400_BAD_REQUEST)
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        serializer.validated_data['cabbie'] = cabbie.first()
        self.perform_update(serializer)

        regular_trip = get_object_or_404(RegularTrip, pk=kwargs['pk'])
        create_order_regular_trip(regular_trip)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)


class DaysRegularTripViewSet(APIView):
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(manual_parameters=[QUERY_REGULAR_TRIP_ID])
    def get(self, request, *args, **kwargs):
        regular_trip_id = self.request.query_params.get('regular_trip_id')
        if not regular_trip_id:
            return Response({'message': 'Doas not exists regular trip id'}, status=status.HTTP_400_BAD_REQUEST)
        regular_trip = get_object_or_404(RegularTrip, pk=regular_trip_id)

        week_days = RegularTripSchedule.objects.filter(trip=regular_trip)

        result = set(str(WeekDayChoices(schedule.week_day).label) for schedule in week_days)
        return Response(result, status=status.HTTP_200_OK)
