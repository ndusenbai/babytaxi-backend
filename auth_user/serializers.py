import os

from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from auth_user.models import User, ExtraInfo, Cabbie
from common.serializers import BaseSerializer
from config import settings


class TokenCredentialsSerializer(TokenObtainPairSerializer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not kwargs.get('data'):  # Just for Swagger
            self.fields['phone_number'] = serializers.CharField(required=False)
            self.fields['email'] = serializers.CharField(required=False)


class UserModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'phone_number', 'email', 'type', 'avatar')
        read_only_fields = ('id', 'is_admin')


class UserProfileSerializer(serializers.ModelSerializer):
    avatar_url = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = User
        exclude = ['is_admin', 'is_staff', 'created_at', 'language', 'groups', 'user_permissions', 'password',]

        extra_kwargs = {
            'password': {'required': False},
            'email': {'required': False},
            'first_name': {'required': False},
            'last_name': {'required': False},
            'phone_number': {'required': False},
            'is_active': {'required': False},
        }

    def get_avatar_url(self, instance):
        if instance.avatar:
            return os.path.join(settings.BASE_URL,  instance.avatar.name)


class RegisterSerializer(serializers.ModelSerializer):
    middle_name = serializers.CharField(max_length=20, allow_blank=True)
    avatar = serializers.FileField(required=False)
    password = serializers.CharField(write_only=True, required=True)
    email = serializers.CharField(required=True, max_length=25)

    class Meta:
        model = User
        exclude = ['created_at',
                   'is_superuser',
                   'is_admin',
                   'is_active',
                   'is_staff',
                   'language',
                   'groups',
                   'user_permissions',
                   'last_login',
                   'pending_password']

        extra_kwargs = {
            'type': {'required': True, 'help_text': 'COBBIE=0, CUSTOMER=1'},
        }


class RegisterVarifyCodeSerializer(BaseSerializer):
    token = serializers.CharField(max_length=32, required=True)
    code = serializers.CharField(max_length=4, required=True)


class SendNumberSerializer(BaseSerializer):
    number = serializers.CharField(max_length=30, required=True)


class PasswordVarifyCodeSerializer(BaseSerializer):
    token = serializers.CharField(max_length=32, required=True)
    code = serializers.CharField(max_length=4, required=True)
    password = serializers.CharField(max_length=30, required=True)


class ExtraInfoCabbieSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExtraInfo
        fields = '__all__'


class CabbieSerializer(serializers.ModelSerializer):
    full_name = serializers.SerializerMethodField(read_only=True)
    phone_number = serializers.SerializerMethodField(read_only=True)
    extra_info = ExtraInfoCabbieSerializer(read_only=True)

    class Meta:
        model = Cabbie
        exclude = ('created_at', 'updated_at')

    def get_phone_number(self, instance: Cabbie):
        return instance.user.phone_number

    def get_full_name(self, instance: Cabbie):
        return instance.user.full_name


class CabbieActionSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('is_active',)
