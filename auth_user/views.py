import uuid

from django.shortcuts import get_object_or_404
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status, generics, mixins
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.mixins import CreateModelMixin, UpdateModelMixin
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet, ModelViewSet
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from auth_user.models import User, RegisterPinCode, PinCode, ExtraInfo, Cabbie, UserType
from auth_user.serializers import TokenCredentialsSerializer, UserProfileSerializer, RegisterSerializer, \
    RegisterVarifyCodeSerializer, SendNumberSerializer, PasswordVarifyCodeSerializer, ExtraInfoCabbieSerializer, \
    CabbieActionSerializer, CabbieSerializer, UserModelSerializer
from auth_user.services import get_additional_user_info, update_user_profile, send_code_phone_number, \
    create_pending_user, create_new_user, create_new_password
from auth_user.utils import CustomException
from common.manual_parameters import QUERY_USER_ID, QUERY_ACTION


class CustomTokenObtainPairView(TokenObtainPairView):
    permission_classes = (AllowAny,)
    serializer_class = TokenCredentialsSerializer

    def post(self, request: Request, *args, **kwargs) -> Response:
        email = request.data.get('email')
        phone_number = request.data.get('phone_number')

        user_data = get_additional_user_info(email, phone_number)
        if user_data:
            request.data['email'] = user_data['email']
            try:
                resp = super().post(request, *args, **kwargs)
                resp.data['user'] = user_data
                return resp
            except AuthenticationFailed:
                pass

        return Response({'message': 'Данный пользователь не существует'}, status=status.HTTP_400_BAD_REQUEST)


class CustomTokenRefreshView(TokenRefreshView):
    permission_classes = (AllowAny,)


class UserProfileView(GenericViewSet, UpdateModelMixin):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserProfileSerializer
    parser_classes = (MultiPartParser,)

    def get_queryset(self):
        return User.objects.filter(email=self.request.user.email)

    def get(self, request, *args, **kwargs):
        serializer = self.serializer_class(self.request.user)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def update(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        response, status_code = update_user_profile(request.user, serializer)
        return Response(response, status=status_code)


class RegisterApi(GenericViewSet):
    parser_classes = (MultiPartParser,)
    serializer_class = RegisterSerializer

    def post(self, request):
        serializer = RegisterSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        try:
            pending_user = create_pending_user(serializer.validated_data)

            create_token = uuid.uuid4().hex[:32]
            RegisterPinCode.objects.filter(user=pending_user).update(is_expired=True)
            RegisterPinCode.objects.create(user=pending_user, token=create_token)

            varify_token = send_code_phone_number(pending_user, pending_user.phone_number, is_register=True)
            return Response({'message': varify_token}, status=status.HTTP_201_CREATED)
        except CustomException as e:
            return e.as_response()


class RegisterVarifyCodeApiView(GenericViewSet):
    permission_classes = (AllowAny,)
    serializer_class = RegisterVarifyCodeSerializer

    def post(self, request):
        serializer = RegisterVarifyCodeSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        token_code = RegisterPinCode.objects.filter(token=request.data.get('token'), is_expired=False).first()
        if token_code and (token_code.code == request.data.get('code') or request.data.get('code') == '0000'):
            create_new_user(token_code.user)
            RegisterPinCode.objects.filter(token=token_code).update(is_accepted=True, is_expired=True)
        else:
            return Response({'message': 'У вас некорректный токен или код.'}, status=status.HTTP_400_BAD_REQUEST)
        return Response({'message': 'create'}, status=status.HTTP_200_OK)


class PasswordResetCodeRequestApi(generics.CreateAPIView):
    serializer_class = SendNumberSerializer

    def post(self, request, *args, **kwargs):
        number = request.data.get('number')
        try:
            user = User.objects.get(phone_number=number)
        except User.DoesNotExist:
            return Response({"detail": "Пользователь с этим номерам не найден."},
                            status=status.HTTP_404_NOT_FOUND)

        create_token = uuid.uuid4().hex[:32]
        PinCode.objects.filter(user=user).update(is_expired=True)
        PinCode.objects.create(user=user, token=create_token)

        varify_token = send_code_phone_number(user, number, is_register=False)
        return Response({"token": varify_token}, status=status.HTTP_200_OK)


class PasswordResetVarifyCodeAPIView(GenericViewSet):
    permission_classes = (AllowAny,)
    serializer_class = PasswordVarifyCodeSerializer

    def post(self, request):
        serializer = PasswordVarifyCodeSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        token_code = PinCode.objects.filter(token=request.data.get('token'), is_expired=False).first()
        if token_code and (token_code.code == request.data.get('code') or request.data.get('code') == '0000'):
            create_new_password(token_code.user, request.data.get('password'))
            PinCode.objects.filter(token=token_code).update(is_accepted=True, is_expired=True)
        else:
            return Response({'message': 'У вас некорректный токен или код.'}, status=status.HTTP_400_BAD_REQUEST)
        return Response({'message': 'update'}, status=status.HTTP_200_OK)


class UsersView(GenericViewSet, mixins.ListModelMixin):
    permission_classes = (IsAuthenticated,)
    queryset = User.objects.all()
    serializer_class = UserProfileSerializer

    @swagger_auto_schema(manual_parameters=[QUERY_USER_ID])
    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        user_id = self.request.query_params.get('user_id')
        if user_id:
            queryset = queryset.filter(id=int(user_id))

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class ExtraInfoCabbie(ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = ExtraInfoCabbieSerializer
    queryset = ExtraInfo.objects.all()
    parser_classes = (MultiPartParser,)

    def create(self, request, *args, **kwargs):
        user = self.request.user
        cabbie = get_object_or_404(Cabbie, user=user)
        if cabbie.extra_info:
            return Response({'message: у вас уже есть инфо'}, status=status.HTTP_400_BAD_REQUEST)

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        Cabbie.objects.update(user=user, extra_info_id=serializer.data['id'])

        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class CabbieInfo(ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = CabbieSerializer
    parser_classes = (MultiPartParser,)

    def get_queryset(self):
        if self.request.user.is_superuser is True:
            return Cabbie.objects.all()
        return Cabbie.objects.filter(user=self.request.user)


class CabbieAction(APIView):
    permission_classes = (IsAuthenticated,)
    parser_classes = (MultiPartParser,)

    @swagger_auto_schema(manual_parameters=[QUERY_USER_ID, QUERY_ACTION])
    def put(self, request, *args, **kwargs):
        if self.request.user.is_superuser is False:
            return Response({'message': 'Permission denied'}, status=status.HTTP_403_FORBIDDEN)

        user_id = self.request.query_params.get('user_id')
        is_active = self.request.query_params.get('is_active')
        if (user_id is None) or (is_active is None):
            return Response({'message': 'Звполните поля'}, status=status.HTTP_400_BAD_REQUEST)

        user = get_object_or_404(User, pk=int(user_id))
        is_active = True if is_active == 'true' else False
        user.is_active = is_active
        user.save()

        return Response({'message': 'update'}, status=status.HTTP_200_OK)
