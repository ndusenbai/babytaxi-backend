from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User, Cabbie, ExtraInfo


@admin.register(User)
class UserAdmin(UserAdmin):
    list_display = ('id', 'last_name', 'email', 'is_active', 'type')
    search_fields = ('id', 'last_name', 'first_name', 'phone_number')

    ordering = ("email",)

    fieldsets = (
        (None, {"fields": ("phone_number", "password")}),
        (
            "Personal info",
            {
                "fields": (
                    "first_name",
                    "last_name",
                    "avatar",
                    "language",
                    "email",
                    "type"
                )
            }
        ),
        (
            "Permissions",
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "is_admin",
                ),
            },
        ),
        (
            "Important dates",
            {
                "fields": (
                    "last_login",
                    "created_at",
                )
            }
        ),
    )
    readonly_fields = ('created_at',)


@admin.register(ExtraInfo)
class CabbieExtraInfo(admin.ModelAdmin):
    list_display = ('id', 'car_model', 'car_number')
    search_fields = ('car_model', 'car_number')
    list_filter = ('id',)


@admin.register(Cabbie)
class CabbieAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'type', 'grade', 'is_active')
    search_fields = ('user', 'type')
    list_filter = ('id',)
