from django.conf.urls.static import static
from django.urls import path
from auth_user import views
from rest_framework.routers import DefaultRouter

from config import settings

router = DefaultRouter()
router.register('extra-info-cabbie', views.ExtraInfoCabbie, basename='extra-info-cabbie')
router.register('cabbie', views.CabbieInfo, basename='cabbie-info')


urlpatterns = [
    path('auth/token/', views.CustomTokenObtainPairView.as_view(), name='token-obtain-pair'),
    path('auth/token/refresh/', views.CustomTokenRefreshView.as_view(), name='token-refresh'),
    path('auth/register/', views.RegisterApi.as_view({'post': 'post'}), name='register'),
    path('auth/register-varify-code/', views.RegisterVarifyCodeApiView.as_view({'post': 'post'}), name='register'),
    path('auth/password-reset-send-code/', views.PasswordResetCodeRequestApi.as_view(),
         name='password-reset-request'),
    path('auth/password-varify-code/', views.PasswordResetVarifyCodeAPIView.as_view({'post': 'post'}),
         name='password-reset-varify'),
    path('users/', views.UsersView.as_view({'get': 'list'}), name='users'),
    path('cabbie/is-active/', views.CabbieAction.as_view(), name='cabbie-is-active'),

    path('user-profile/', views.UserProfileView.as_view({'get': 'get', 'patch': 'update'}), name='user-profile'),
] + router.urls
