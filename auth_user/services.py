from rest_framework import serializers
from typing import Optional

from django.contrib.auth.hashers import make_password

from auth_user.models import User, RegisterPinCode, PinCode, PendingUser, UserType, Cabbie, ExtraInfo
from auth_user.serializers import UserModelSerializer
from auth_user.utils import PhoneNumberExistsException, EmailExistsException


def get_additional_user_info(email: str, phone_number: str) -> Optional[dict]:
    user = User.objects.filter(email=email).first() if email else None
    if not user:
        user = User.objects.filter(phone_number=phone_number).first()
    if not user:
        return None

    user_data = UserModelSerializer(user).data

    return user_data


def update_user_profile(user, serializer):
    data = serializer.validated_data
    serializer = UserModelSerializer(user, data=data, partial=True)
    serializer.is_valid(raise_exception=True)
    serializer.save()

    return serializer.data, 200


def send_code_phone_number(user, number, is_register):
    if is_register:
        pin_code = RegisterPinCode.objects.filter(user=user).order_by('-created_at').first()
    else:
        pin_code = PinCode.objects.filter(user=user).order_by('-created_at').first()
    user = pin_code.code
    token = pin_code.token

    # account_sid = 'ACaf6cf18213ec025b707ab4c04fb5d30e'
    # auth_token = '1bb459b6a01bf3ab81d18f6a3d4f4fad'
    # client = Client(account_sid, auth_token)
    #
    # message = client.messages.create(
    #     from_='+14437313193',
    #     body=f'Your code: {str(code)}',
    #     to=number
    # )

    return token


def create_pending_user(validated_data: dict) -> PendingUser:

    if User.objects.filter(phone_number=validated_data['phone_number']).exists():
        raise PhoneNumberExistsException()

    if User.objects.filter(email=validated_data['email']).exists():
        raise EmailExistsException()

    user = PendingUser.objects.filter(phone_number=validated_data['phone_number']).first()
    if user:
        user.delete()

    user = PendingUser()
    user.first_name = validated_data['first_name']
    user.last_name = validated_data['last_name']
    user.middle_name = validated_data['middle_name']
    user.phone_number = validated_data['phone_number']
    user.password_hash = validated_data['password']
    user.email = validated_data['email']
    user.avatar = validated_data.get('avatar')
    user.type = validated_data['type']
    user.save()

    return user


def create_new_user(user: PendingUser) -> User:
    custom_user = User()
    custom_user.email = user.email
    custom_user.first_name = user.first_name
    custom_user.last_name = user.last_name
    custom_user.middle_name = user.middle_name
    custom_user.phone_number = user.phone_number
    custom_user.pending_password = user.password_hash
    custom_user.password = make_password(user.password_hash)
    custom_user.avatar = user.avatar
    if user.type == UserType.CABBIE:
        custom_user.type = UserType.CABBIE
    custom_user.save()

    if user.type == UserType.CABBIE:
        extra_info = ExtraInfo()
        extra_info.save()

        cabbie = Cabbie()
        cabbie.user = custom_user
        cabbie.extra_info = extra_info
        cabbie.save()

    return custom_user


def create_new_password(user, password):
    User.objects.filter(email=user.email).update(password=make_password(password))
