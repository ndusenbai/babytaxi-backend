import random
from datetime import timedelta, datetime

from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db.models import CASCADE

from imagekit import processors
from imagekit.models import ProcessedImageField
from django.db import models
from django.utils import timezone

from common.models import BaseModel


class UserManager(BaseUserManager):

    def create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError("The Email must be set")
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)
        extra_fields.setdefault("is_active", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")
        return self.create_user(email, password, **extra_fields)


class UserType(models.IntegerChoices):
    CABBIE = 0, 'cabbie'
    CUSTOMER = 1, 'customer'


class User(AbstractBaseUser, PermissionsMixin):
    type = models.IntegerField(
        choices=UserType.choices,
        validators=[MinValueValidator(0), MaxValueValidator(1)],
        default=UserType.CUSTOMER,
    )
    email = models.EmailField(max_length=70, unique=True, blank=True, null=True)
    first_name = models.CharField(max_length=50, blank=True)
    last_name = models.CharField(max_length=50, blank=True)
    phone_number = models.CharField(max_length=15, blank=False)
    avatar = ProcessedImageField(
        upload_to='avatar/',
        processors=[processors.Transpose()],  # transpose - to fix the 90˚ rotation issue
        options={'quality': 60},
        null=True,
        blank=True,
    )
    is_superuser = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    pending_password = models.CharField(blank=True)

    language = models.CharField(max_length=10, default='ru')
    created_at = models.DateTimeField(auto_now=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'

    @property
    def full_name(self):
        full_name = f"{self.last_name or ''} {self.first_name or ''}".strip()
        return full_name

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'


class CabbieType(models.IntegerChoices):
    STANDARD = 0, 'standard'
    LUX = 1, 'lux',
    CHILDISH = 2, 'childish'


class PendingCabbie(BaseModel):
    user = models.ForeignKey(User, on_delete=CASCADE)
    driver_license_series = models.CharField(blank=True)
    is_active = models.BooleanField(default=False)


class PendingUser(BaseModel):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    middle_name = models.CharField(max_length=50, blank=True)
    email = models.EmailField(max_length=70)
    phone_number = models.CharField(max_length=32)
    password_hash = models.CharField('password', max_length=128)
    type = models.IntegerField(
        choices=UserType.choices,
        validators=[MinValueValidator(0), MaxValueValidator(1)],
        default=UserType.CUSTOMER,
    )
    avatar = ProcessedImageField(
        upload_to='avatar/',
        processors=[processors.Transpose()],  # transpose - to fix the 90˚ rotation issue
        format='WEBP',
        options={'quality': 60},
        null=True,
        blank=True
    )


class ExtraInfo(BaseModel):
    car_model = models.CharField(max_length=50, null=True)
    car_number = models.CharField(max_length=15, null=True)

    transportation_license = ProcessedImageField(
        upload_to='transportation-license/',
        processors=[processors.Transpose()],  # transpose - to fix the 90˚ rotation issue
        format='WEBP',
        options={'quality': 60},
        null=True,
        blank=True
    )
    identification_card = ProcessedImageField(
        upload_to='identification-card/',
        processors=[processors.Transpose()],  # transpose - to fix the 90˚ rotation issue
        format='WEBP',
        options={'quality': 60},
        null=True,
        blank=True
    )
    criminal_record_certificate = ProcessedImageField(
        upload_to='criminal-record-certificate/',
        processors=[processors.Transpose()],  # transpose - to fix the 90˚ rotation issue
        format='WEBP',
        options={'quality': 60},
        null=True,
        blank=True
    )
    drug_dispensary_certificate = ProcessedImageField(
        upload_to='drug-dispensary-certificate/',
        processors=[processors.Transpose()],  # transpose - to fix the 90˚ rotation issue
        format='WEBP',
        options={'quality': 60},
        null=True,
        blank=True
    )
    psychiatric_clinic_certificate = ProcessedImageField(
        upload_to='psychiatric-clinic-certificate/',
        processors=[processors.Transpose()],  # transpose - to fix the 90˚ rotation issue
        format='WEBP',
        options={'quality': 60},
        null=True,
        blank=True
    )


class Cabbie(BaseModel):
    user = models.ForeignKey(User, on_delete=CASCADE)
    extra_info = models.ForeignKey(ExtraInfo, on_delete=CASCADE, null=True)
    type = models.IntegerField(
        choices=CabbieType.choices,
        validators=[MinValueValidator(0), MaxValueValidator(2)],
        default=CabbieType.STANDARD,
    )
    balance = models.PositiveIntegerField(default=0)
    grade = models.DecimalField(max_digits=5, decimal_places=2, default=5)
    is_active = models.BooleanField(default=False)


class PinCode(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='pin_codes')
    code = models.CharField(max_length=4, blank=True)
    token = models.CharField(max_length=32, blank=True)
    expiration = models.DateTimeField(blank=True, editable=False)
    is_accepted = models.BooleanField(default=False)
    is_expired = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.user}, Code: {self.code}, Accepted: {self.is_accepted}"

    def set_accept_code(self):
        self.code = str(random.randint(1000, 9999))

    def save(self, *args, **kwargs):
        if not self.code:
            self.expiration = datetime.now() + timedelta(days=1)
            self.set_accept_code()
        super(PinCode, self).save(*args, **kwargs)

    class Meta:
        ordering = ['-created_at']
        unique_together = ['code', 'is_accepted']
        verbose_name = "Код для восстановления пароля"
        verbose_name_plural = "Коды для восстановления пароля"


class RegisterPinCode(BaseModel):
    user = models.ForeignKey(PendingUser, on_delete=models.CASCADE, related_name='pin_codes')
    code = models.CharField(max_length=4, blank=True)
    token = models.CharField(max_length=32, blank=True)
    expiration = models.DateTimeField(blank=True, editable=False)
    is_accepted = models.BooleanField(default=False)
    is_expired = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.user}, Code: {self.code}, Accepted: {self.is_accepted}"

    def set_accept_code(self):
        self.code = str(random.randint(1000, 9999))

    def save(self, *args, **kwargs):
        if not self.code:
            self.expiration = datetime.now() + timedelta(days=1)
            self.set_accept_code()
        super(RegisterPinCode, self).save(*args, **kwargs)

    class Meta:
        ordering = ['-created_at']
        unique_together = ['code', 'is_accepted']
        verbose_name = "Код для register"
        verbose_name_plural = "Коды для register"
